#### 版本迭代
- v1.0.0
- 已实现功能
  1. 支持顶部弹幕
  2. 支持底部弹幕
  3. 支持左右滚动弹幕
  4. 支持特殊弹幕
  5. 支持弹幕播放暂停继续
  6. 支持纯文本弹幕
  7. 支持弹幕隐藏显示
  8. 支持设置弹幕显示区域大小