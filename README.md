# DanmakuFlameMaster

## 简介
> DanmakuFlameMaster是一款弹幕框架，支持发送纯文本弹幕、设置弹幕显示区域、控制弹幕播放状态等功能
<img src="./gifs/演示效果.gif"/>

## 下载安装
````
npm install @ohos/danmakuflamemaster --save
````

OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

### 初始化：并设置弹幕显示相关属性
```
this.model.setWidth(lpx2px(1280))//设置弹幕显示区域宽度
    this.model.setHeight(lpx2px(720))//设置弹幕显示区域高度
    let maxLinesPair: Map<number, number> = new Map();
    maxLinesPair.set(BaseDanmaku.TYPE_SCROLL_RL, 5); // 滚动弹幕最大显示5行
    // 设置是否禁止重叠
    let overlappingEnablePair: Map<number, boolean> = new Map();
    overlappingEnablePair.set(BaseDanmaku.TYPE_SCROLL_RL, true);//设置是否显示从右到左滚动弹幕
    overlappingEnablePair.set(BaseDanmaku.TYPE_FIX_TOP, true);//设置是否显示顶部固定弹幕
    this.mContext = DanmakuContext.create();
    this.mContext.setDanmakuStyle(DANMAKU_STYLE_STROKEN, 3)
      .setDuplicateMergingEnabled(false)
      .setScrollSpeedFactor(1.2)
      .setScaleTextSize(1.2)
      .setCacheStuffer(new SimpleTextCacheStuffer(), this.mCacheStufferAdapter) // 图文混排使用SpannedCacheStuffer
      .setMaximumLines(maxLinesPair)
      .preventOverlapping(overlappingEnablePair)
      .setDanmakuMargin(40);
    let that = this
    if (this.model != null) {
      this.mParser = this.createParser();
      this.model.setCallback(new class implements Callback {
        public updateTimer(timer: DanmakuTimer): void {
        }

        public drawingFinished(): void {

        }

        public danmakuShown(danmaku: BaseDanmaku): void {
        }

        public prepared(): void {
          that.model.start();
        }
      });

      this.model.setOnDanmakuClickListener(new class implements OnDanmakuClickListener{
        onDanmakuClick(danmakus: IDanmakus): boolean{
          console.log('DFM onDanmakuClick: danmakus size:'+danmakus.size())
            let latest:BaseDanmaku = danmakus.last()
            if(null!=latest){
              console.log('DFM onDanmakuClick: text of latest danmaku:'+latest.text)
                return true
            }
            return false
        };

        onDanmakuLongClick(danmakus: IDanmakus): boolean{
            return false
        };

        onViewClick(view: IDanmakuView): boolean{
            that.isVisible = true
            return false
        };
      })
      this.model.prepare(this.mParser, this.mContext);
      this.model.showFPS(true);

```
### 添加弹幕
```
let danmaku: BaseDanmaku = this.mContext.mDanmakuFactory.createDanmaku(BaseDanmaku.TYPE_SCROLL_RL);
    danmaku.text = "这是一条弹幕" + SystemClock.uptimeMillis();
    danmaku.padding = 5;
    danmaku.priority = 0; // 可能会被各种过滤器过滤并隐藏显示
    danmaku.isLive = isLive.valueOf();
    danmaku.setTime(this.model.getCurrentTime() + 1200);
    danmaku.textSize = 25 * (this.mParser.getDisplayer().getDensity() * 0.8);
    danmaku.textColor = 0xffff0000;
    danmaku.textShadowColor = 0xffffffff;
    danmaku.borderColor = 0xff00ff00;
    this.model.addDanmaku(danmaku);
```
## 接口说明
` model: DanmakuView.Model = new DanmakuView.Model()`
1. 添加弹幕
   `model.addDanmaku(danmaku)`
2. 获取当前弹幕时间
   `model.getCurrentTime()`
3. 隐藏弹幕显示
   `model.hide()`
4. 弹幕显示
   `model.show()`
5. 弹幕暂停播放
   `model.pause()`
6. 弹幕继续播放
   `model.resume()`
7. 设置弹幕显示区域宽度
   `model.setWidth(lpx2px(1280))`
8. 设置弹幕显示区域高度
   `model.setHeight(lpx2px(720))`
9. 启动弹幕播放
   `model.prepare(this.mParser, this.mContext)`
10. 显示弹幕播放的fps
    `model.showFPS(true)`
11. 设置弹幕点击回调
    `model.setOnDanmakuClickListener()`

## 兼容性
支持 OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- DanmakuFlameMaster  
|     |---- entry  # 示例代码文件夹
|     |---- DanmakuFlameMaster  # DanmakuFlameMaster库文件夹
|           |---- src\main\ets\components\common\master\flame\danmaku  # 源代码文件夹
|           		|---- control   # 弹幕状态控制实现
|           		|---- danmaku 	# 弹幕基础类库
|           		|---- ui 		# 弹幕自定义显示控件
|           |---- index.ets  # 对外接口
|     |---- README.MD  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/DanmakuFlameMaster/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/DanmakuFlameMaster/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/DanmakuFlameMaster/blob/master/LICENSE) ，请自由地享受和参与开源。